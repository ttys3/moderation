# Development with Docker (unofficial)

> **Note:** This guide is not audited or maintained by Codeberg but the community
> Use everything outside the official instructions at their own risk.

This uses docker to keep things consistent across platforms and create an easier way to get started.

## Get started

**1. Start gitea service**

:file_folder: Please `cd` into `docker`.

Start the gitea docker service:

```
$ docker-compose up -d gitea
```

Open a browser and open <http://localhost:3000>.

Inside the initial configuration steps leave the defaults, except for the following:

*Administrator Account Settings*

| Label | Text Input |
|---|---|
| Administrator Username | `admin1234`        |
| Password               | `password`         |
| Confirm Password       | `password`         |
| Email Address          | `user@example.com` |

**2. Copy the backend configuration**

:file_folder: Please `cd` into `backend`.

Copy the example configuration file:
```
$ cp moderation_backend.ini.example moderation_backend.ini
```

Edit the `URL` under `[gitea]` in `moderation_backend.ini` to `http://gitea:3000`

**3. Retrieve API Token**

```
$ curl -XPOST -H "Content-Type: application/json"  -k -d '{"name":"moderation"}' -u admin1234:password http://localhost:3000/api/v1/users/admin1234/tokens
```

Copy the `sha1` value from the json response, into `API_TOKEN` under `[gitea]` in `moderation_backend.ini`

**4. Set the jwt token**

```
openssl rand -base64 32
```

Copy the response to the `JWT_SECRET` under `[security]` in `moderation_backend.ini`, make sure to wrap it in double-quotes (`"`)

**5. Start backend service**

:file_folder: Please `cd` to `docker`.

Run the following to start a golang container, that live reloads a `go run .` command on `.go` file changes:
```
$ docker-compose up -d backend-live
```

And open with your preferred browser on <http://127.0.0.1:3333>

The frontend if served via the backend router.

## Continue where you left off

```
cd docker
docker-compose up -d
```

## Ports used

| Description | Port |
|---|---:|
| Moderation | [3333](http://127.0.0.1:3333) |
| Gitea      | [3000](http://127.0.0.1:3000) |

## Uninstall

To remove `devd` binary, run `which devd`, then `rm <result>`.

To remove any trace of the docker services run the following command:

```
cd docker
docker-compose down --volumes --rmi all
rm -rf .gitea
```
