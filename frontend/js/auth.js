var jwtToken;

document.getElementById("cb-auth-login").addEventListener("click", function(e){
	auth();
});

let auth = function() {
	fetch('/auth/login').then(response =>{
		return response.text().then(function(text) {
			jwtToken = text;
			document.getElementById("cb-auth").innerHTML = "Logged in :)";
			return getUser();
		});
	});
}

let getUser = function() {
	return fetch('/user', {
		headers: {
			"Authorization": "BEARER " + jwtToken
		}
	}).then(response => {
		if (response.status == 401) {
			return new Promise(function(){
				console.log("not logged in");
			});
		}

		return response.text().then(function(text) {
			let oauthLoginLink = document.getElementById("login-button");
			oauthLoginLink.innerHTML = text;
			oauthLoginLink.href = "#";

			let loginButton = document.getElementById("cb-auth-login")
			loginButton.disabled = true;
		});
	});
}

getUser();
