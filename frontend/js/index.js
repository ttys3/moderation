// alias
function elId(id) {
	return document.getElementById(id);
}

function elClass(classname) {
	return document.getElementsByClassName(classname);
}

var input = {type: undefined, list: [], renderTarget: elId("cb-input-rendered")};
var output = {...input, renderTarget: elId("cb-output-rendered")};

// reads user input and splits into field array
function getInput() {
	return elId("cb-input-list").value.replace(/^(https:\/\/[^/]*|)\//gm,"").split('\n')
		.filter(line => {
			return line != "";
		})
		.map(url => {
			return url.split("/");
		});
}

// determines which type a user input is (e.g. users or repos or issues etc)
function getInputType(inputArr) {
	while ((inputArr[0].length == 0 || inputArr[0][0] == "") && i < inputArr.length) {
		inputArr.shift();
	}
	switch (inputArr[0].length) {
		case 1:
			return "users";
			break;
		case 2:
			return "repos";
			break;
	}
}

function resetList(list) {
	list.type = undefined;
	list.list = [];
	render(list);
}

function takeList(list) {
	if (list.type != input.type) {
		input.type = list.type;
		input.list = [];
	}
	input.list = input.list.concat(list.list);
	render(input);
}

// turns manually entered input into the input list
elId("cb-input-list").addEventListener("keypress", function(e) {
	if (e.key === 'Enter') {
		inputArr = getInput();
		if (input.type === undefined) {
			input.type = getInputType(inputArr);
			input.list = [];
		}
		elId("cb-input-list").value="";

		let newInput;

		switch (input.type) {
			case "users":
				newInput = inputArr.map(fields => { return { login: fields[0] }});
			break;
			case "repos":
				newInput = inputArr.map(fields => { return { owner: fields[0], name: fields[1] }});
			break;
		}

		input.list = input.list.concat(newInput);
		render(input);
	}
});

Array.from(elClass("sidebar-link")).forEach(function(link){
	link.addEventListener("click", loadFunction)
});

function loadFunction() {
	let fn = this.id.replace("cb-sb-","");
	elId("cb-function").innerHTML=elId("cb-fn-" + fn).innerHTML;
}

function callBackend(action, callback, properties = {}) {
	let request = {};

	elId("cb-input").querySelectorAll('[id^=cb-input-]').forEach(function(element){
		fieldname = element.id.replace("cb-input-","");
		if (element.getAttribute("type") == "checkbox") {
			request[fieldname] = element.checked;
		} else {
			request[fieldname] = element.value;
		}
	});

	if (properties.needsConfirmation && !confirm ("Do you really want to call " + action + "?")) {
		return false;
	}

	requestProperties = {
		headers: {
			"Authorization": "BEARER " + jwtToken
		}
	}
	if (input.type !== undefined && input.list !== undefined && input.type == properties.inputType) {
		request.list = input.list;
		requestProperties = {...requestProperties,
			method: "POST",
			body: JSON.stringify(request)
		};
	}
	fetch(action, requestProperties
	).then(response =>{
		return response.json();
	}).then(data =>{
		if (typeof(callback) !== 'undefined') {
			callback(data);
		}
	});
}

