package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-chi/render"
	"github.com/go-ini/ini"
	"github.com/lestrrat-go/jwx/jwa"

	"codeberg.org/codeberg/moderation/internal/log"
	"codeberg.org/codeberg/moderation/internal/mailer"
)

/* order of file headers
 * - imports
 * - globals, consts
 * - type definitions
 * - routes
 * - config function
 */

/* common globals
 * globals that are only accessed in a single file are defined there
 * using a namespace alike filenameConstantname
 */

var (
	jwtAuth     *jwtauth.JWTAuth
	jwtAlg      jwa.SignatureAlgorithm = jwa.HS256
	giteaURL    string
	giteaClient *gitea.Client
)

type PermissionLevel int

const (
	PERMISSION_USER   PermissionLevel = iota + 1 // nolint:deadcode
	PERMISSION_MEMBER                            // nolint:deadcode
	PERMISSION_MODERATOR
	PERMISSION_ADMIN
)

type GenericRequest struct {
	ExpandRepoActionToForks    bool   `json:"expandforks"`
	InformUser                 bool   `json:"informuser"`
	InformUserAdditionalReason string `json:"informuserreason"`
	InternalActionComment      string `json:"internalcomment"`
}

type GenericError struct {
	Action string
	Result string
}

func (err *GenericError) Error() string {
	return err.Action + ": " + err.Result
}

func main() {
	// configuration
	cfg, err := ini.Load("moderation_backend.ini")
	if err != nil {
		log.Fatal("ini.Load: %v", err)
	}

	appListenUrl := cfg.Section("moderation_app").Key("LISTEN_URL").String()
	if appListenUrl == "" {
		log.Fatal("Please make sure you have a listen url in your moderation_backend.ini")
	}

	giteaToken := cfg.Section("gitea").Key("API_TOKEN").String()
	giteaURL = cfg.Section("gitea").Key("URL").String()
	if giteaToken == "" || giteaURL == "" {
		log.Fatal("Please make sure you have token and url in your moderation_backend.ini")
	}

	jwtSecret := cfg.Section("security").Key("JWT_SECRET").String()
	if jwtSecret == "" || jwtSecret == "default_jwt_secret" {
		log.Fatal("Please make sure you set a unique jwt secret in the moderation_backend.ini")
	}
	userMap := make(map[string]string)
	for _, user := range cfg.Section("security.users").Keys() {
		userMap[user.Name()] = user.String()
	}
	if len(userMap) == 0 {
		log.Warn("You should add at least one user under the [user] section in moderation_backend.ini")
	}
	mailer.Config(cfg)
	userConfig(cfg)
	repoConfig(cfg)
	log.Config(cfg)

	// inits
	giteaClient, err = gitea.NewClient(giteaURL, gitea.SetToken(giteaToken))
	if err != nil {
		log.Fatal("Could not connect[url=%q]: %v", giteaURL, err)
	}
	jwtAuth = jwtauth.New(string(jwtAlg), []byte(jwtSecret), nil)

	// chi routing etc
	flag.Parse()

	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(jwtauth.Verifier(jwtAuth))

	r.Get("/authorize", authorize(cfg))
	r.Get("/oauth", oauthCallback(cfg))
	r.Get("/user", userHandler)

	r.Route("/users", userRoutes)
	r.Route("/repos", repoRoutes)
	r.Group(func(r chi.Router) {
		r.Use(middleware.BasicAuth("user", userMap))
		r.Get("/auth/login", login)
	})

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "../frontend"))

	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})

	log.Info("Starting app now on %s", appListenUrl)
	log.Fatal("http: %v", http.ListenAndServe(appListenUrl, r))
}

func login(w http.ResponseWriter, r *http.Request) {
	_, newToken, _ := jwtAuth.Encode(map[string]interface{}{"p": PERMISSION_ADMIN, "sub": ".ini User"})
	_, _ = w.Write([]byte(newToken))
}

func authRequire(r *http.Request, requiredPermissionLevel PermissionLevel) error {
	token, err := VerifyRequest(r)
	if err != nil {
		return fmt.Errorf("jwtauth.FromContext: %v", err)
	}

	permission, exists := token.Get("p")

	if !exists {
		return &GenericError{
			Action: "Authenticate",
			Result: "No token",
		}
	}

	permissionLevel := PermissionLevel(permission.(float64))

	log.Info("DEBUG: permission found: %v", permissionLevel)

	if permissionLevel < requiredPermissionLevel {
		return &GenericError{
			Action: "Authenticate",
			Result: fmt.Sprintf("Authentication level of %d does not match required level of %d", permissionLevel, requiredPermissionLevel),
		}
	}
	return nil
}

func filterInto(source, targetStruct interface{}) (interface{}, error) {
	sourceSlice := reflect.ValueOf(source)
	targetType := reflect.TypeOf(targetStruct)
	targetVal := reflect.ValueOf(targetStruct)
	if sourceSlice.Kind() != reflect.Ptr || targetType.Kind() != reflect.Ptr {
		return nil, &GenericError{Result: "No pointer"}
	}
	sourceSlice = sourceSlice.Elem()
	targetType = targetType.Elem()
	targetVal = targetVal.Elem()
	if sourceSlice.Kind() != reflect.Slice || targetType.Kind() != reflect.Struct || targetVal.Kind() != reflect.Struct {
		return nil, &GenericError{Result: "No slice"}
	}
	if sourceSlice.Len() == 0 {
		return nil, nil
	}

	var targetFields []reflect.StructField
	for i := 0; i < targetType.NumField(); i++ {
		targetFields = append(targetFields, targetType.Field(i))
	}
	var targetSlice []interface{}
	for i := 0; i < sourceSlice.Len(); i++ {
		newStruct := reflect.New(targetType)
		for j := range targetFields {
			structfield := newStruct.Elem().FieldByName(targetFields[j].Name)
			structfield.Set(sourceSlice.Index(i).Elem().FieldByName(targetFields[j].Name))
		}
		targetSlice = append(targetSlice, newStruct.Interface())
	}
	return targetSlice, nil
}
