package util

import (
	"encoding/json"
	"net/http"
	"strings"

	"codeberg.org/codeberg/moderation/internal/log"
)

func ErrorMiddleware(fn func(w http.ResponseWriter, r *http.Request) error) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := fn(w, r); err != nil {
			errorStr := err.Error()

			// the function should return a error that already has been formatted.
			log.Println(errorStr)

			caller := "?"
			// Split the error with spaces.
			spacedSlice := strings.Split(errorStr, " ")
			if len(spacedSlice) >= 3 {
				// Get the third message, then split it on `:`
				// Get the last(thus third) message which contains the caller.
				caller = strings.Split(spacedSlice[2], ":")[2]
			}

			res, err := json.Marshal(struct {
				Origin  string
				Details string
			}{
				Origin:  caller,
				Details: strings.Join(spacedSlice[2:], " "),
			})
			if err != nil {
				log.Error("Error while encoding error JSON: %v", err)
				return
			}
			_, _ = w.Write(res)
		}
	}
}
