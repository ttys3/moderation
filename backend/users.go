package main

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/chi/v5"
	"github.com/go-ini/ini"

	"codeberg.org/codeberg/moderation/internal/log"
	"codeberg.org/codeberg/moderation/internal/util"
)

var (
	suspiciousUserKeywords []string
	oauthStates            []string
)

type User struct {
	ID          int64     `json:"id"`
	UserName    string    `json:"login"`
	FullName    string    `json:"full_name"`
	Email       string    `json:"email"`
	AvatarURL   string    `json:"avatar_url"`
	LastLogin   time.Time `json:"last_login"`
	Location    string    `json:"location"`
	Description string    `json:"description"`
}

type UserSimple struct {
	ID       int64  `json:"id"`
	UserName string `json:"login"`
	FullName string `json:"full_name"`
}

type UserActionRequest struct {
	GenericRequest
	List []UserSimple `json:"list"`
}

func userRoutes(r chi.Router) {
	r.Get("/list", util.ErrorMiddleware(userList))
	r.Get("/list/suspicious", util.ErrorMiddleware(userListSuspicious))
	r.Post("/delete", util.ErrorMiddleware(userDelete))
}

func userConfig(cfg *ini.File) {
	userconfig := cfg.Section("users")
	suspiciousUserKeywords = userconfig.Key("SUSPICIOUS_USER_KEYWORDS").Strings(",")
}

func userList(w http.ResponseWriter, r *http.Request) error {
	if err := authRequire(r, PERMISSION_MODERATOR); err != nil {
		err = log.NewError("Not able to authenticate user: %v", err)
		w.WriteHeader(http.StatusForbidden)
		return err
	}

	users, _, err := giteaClient.AdminListUsers(gitea.AdminListUsersOptions{})
	if err != nil {
		return log.NewError("Not able to list admin users: %v", err)
	}

	data, err := filterInto(&users, &UserSimple{})
	if err != nil {
		return log.NewError("Not able to filterInto user: %v", err)
	}

	res, err := json.Marshal(data)
	if err != nil {
		return log.NewError("json.Marshal: %v", err)
	}
	_, _ = w.Write(res)
	return nil
}

func userListSuspicious(w http.ResponseWriter, r *http.Request) error {
	if err := authRequire(r, PERMISSION_MODERATOR); err != nil {
		return log.NewError("Not able to authenticate user: %v", err)
	}

	users := []*gitea.User{}

	suspiciousKeyword := make([]string, len(suspiciousUserKeywords))
	copy(suspiciousKeyword, suspiciousUserKeywords)

	// go through the keywords in random order to have alternating results
	rand.Shuffle(len(suspiciousKeyword), func(i, j int) {
		suspiciousKeyword[i], suspiciousKeyword[j] = suspiciousKeyword[j], suspiciousKeyword[i]
	})

	for _, keyword := range suspiciousUserKeywords {
		foundUsers, _, err := giteaClient.SearchUsers(gitea.SearchUsersOption{
			KeyWord: keyword,
		})
		if err != nil {
			return log.NewError("Not able to SearchUsers[keyword=%s]: %v", keyword, err)
		}
		users = append(users, foundUsers...)
		if len(users) >= 100 {
			break
		}
	}
	data, err := filterInto(&users, &User{})
	if err != nil {
		return log.NewError("Not able to filterInto user: %v", err)
	}

	res, err := json.Marshal(data)
	if err != nil {
		return log.NewError("json.Marshal: %v", err)
	}
	_, _ = w.Write(res)
	return nil
}

func userDelete(w http.ResponseWriter, r *http.Request) error {
	if err := authRequire(r, PERMISSION_ADMIN); err != nil {
		return log.NewError("Not able to authenticate user: %v", err)
	}
	request := UserActionRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return log.NewError("Error decoding request's body as JSON: %v", err)
	}
	var errors []GenericError
	for _, user := range request.List {
		if _, err := giteaClient.AdminDeleteUser(user.UserName); err != nil {
			errors = append(errors, GenericError{"Delete User", err.Error()})
		}
	}
	res, err := json.Marshal(errors)
	if err != nil {
		return log.NewError("json.Marshal: %v", err)
	}
	_, _ = w.Write(res)
	return nil
}
