module codeberg.org/codeberg/moderation

go 1.16

require code.gitea.io/sdk/gitea v0.15.1

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/docgen v1.2.0 // indirect
	github.com/go-chi/jwtauth/v5 v5.0.2
	github.com/go-chi/render v1.0.1
	github.com/go-ini/ini v1.66.4
	github.com/hashicorp/go-version v1.4.0 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/jwx v1.2.21 // indirect
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
