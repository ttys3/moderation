package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-ini/ini"
	"github.com/lestrrat-go/jwx/jwt"
)

type TokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Code         string `json:"code"`
	GrantType    string `json:"grant_type"`
	RedirectUri  string `json:"redirect_uri"`
}

type TokenResponse struct {
	AccessToken string `json:"access_token"`
}

func randomString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func authorize(cfg *ini.File) func(http.ResponseWriter, *http.Request) {
	oauthconfig := cfg.Section("oauth")
	clientId := oauthconfig.Key("CLIENT_ID").String()
	redirectUri := oauthconfig.Key("REDIRECT_URI").String()

	if clientId == "" || redirectUri == "" {
		return func(w http.ResponseWriter, r *http.Request) {
			_, _ = w.Write([]byte("oauth not properly configured, check the ini file"))
			w.WriteHeader(http.StatusInternalServerError)
		}
	} else {
		return func(w http.ResponseWriter, r *http.Request) {
			state := randomString(20)
			oauthStates = append(oauthStates, state)
			oauthRedirect := fmt.Sprintf("%s/login/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=%s", giteaURL, clientId, redirectUri, state)

			w.Header().Set("Location", oauthRedirect)
			w.WriteHeader(http.StatusFound)
		}
	}
}

func VerifyRequest(r *http.Request) (jwt.Token, error) {
	tokenString := jwtauth.TokenFromCookie(r)
	if tokenString == "" {
		tokenString = jwtauth.TokenFromHeader(r)
	}
	return jwtauth.VerifyToken(jwtAuth, tokenString)
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	token, err := VerifyRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte("invalid token"))
	} else {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte(token.Subject()))
	}
}

func stateExists(str string) bool {
	for _, state := range oauthStates {
		if state == str {
			return true
		}
	}
	return false
}

func getUserFromGitea(accessToken string) *gitea.User {
	userClient, _ := gitea.NewClient(giteaURL, gitea.SetToken(accessToken))
	user, _, _ := userClient.GetMyUserInfo()
	return user
}

func oauthCallback(cfg *ini.File) func(http.ResponseWriter, *http.Request) {
	oauthconfig := cfg.Section("oauth")
	clientId := oauthconfig.Key("CLIENT_ID").String()
	clientSecret := oauthconfig.Key("CLIENT_SECRET").String()
	redirectUri := oauthconfig.Key("REDIRECT_URI").String()

	return func(w http.ResponseWriter, r *http.Request) {
		code := r.URL.Query().Get("code")
		state := r.URL.Query().Get("state")

		if !stateExists(state) {
			_, _ = w.Write([]byte("invalid state"))
			w.WriteHeader(http.StatusConflict)
			return
		}

		tokenUrl := fmt.Sprintf("%s/login/oauth/access_token", giteaURL)

		tokenRequest := TokenRequest{
			clientId,
			clientSecret,
			code,
			"authorization_code",
			redirectUri,
		}

		tokenRequestJson, _ := json.Marshal(tokenRequest)
		tokenResponse, _ := http.Post(tokenUrl, "application/json", bytes.NewBuffer(tokenRequestJson))

		var tokenResponseJson TokenResponse
		_ = json.NewDecoder(tokenResponse.Body).Decode(&tokenResponseJson)

		user := getUserFromGitea(tokenResponseJson.AccessToken)

		if user.IsAdmin {
			expiration := time.Now().Add(3 * 24 * time.Hour)
			_, newToken, _ := jwtAuth.Encode(map[string]interface{}{"p": PERMISSION_ADMIN, "sub": user.UserName})

			cookie := http.Cookie{Name: "jwt", Value: string(newToken), Expires: expiration, HttpOnly: true, Secure: true}
			http.SetCookie(w, &cookie)
			w.Header().Set("Location", "/?message=login_success")
			w.WriteHeader(http.StatusFound)
		} else {
			w.Header().Set("Location", "/?message=login_forbidden")
			w.WriteHeader(http.StatusFound)
		}
	}
}
